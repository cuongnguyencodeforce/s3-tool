<?php
	require 'vendor/autoload.php';

	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
	use Aws\Iam\IamClient;
	$dotenv = new Dotenv\Dotenv(__DIR__);
	$dotenv->load();
	// AWS Info
	$bucketName = $_ENV['BUCKET_NAME'];
	// Connect to AWS
	try {
		// You may need to change the region. It will say in the URL when the bucket is open
		// and on creation.
		// Get the contents of our bucket
        $bucket_contents = $s3->getBucket($bucketName);
        
        foreach ($bucket_contents as $file){
        
            $fname = $file['name'];
            $furl = "http://jurgens-nettuts-tutorial.s3.amazonaws.com/".$fname;
            
            //output a link to the file
            echo "<a href=\"$furl\">$fname</a><br />";
        }
	} catch (Exception $e) {
		// We use a die, so if this fails. It stops here. Typically this is a REST call so this would
		// return a json object.
		die("Error 2: " . $e->getMessage());
	}
	
?>