<?php
	require 'vendor/autoload.php';

	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
	use Aws\Iam\IamClient;
	$dotenv = new Dotenv\Dotenv(__DIR__);
	$dotenv->load();
	$bucketName = $_ENV['BUCKET_NAME'];

	$keyName = $_FILES['uploadfile']['name'] ? $_FILES['uploadfile']['name'] : $_POST['key'];
	$path    = $_POST['path'];
	$action  = $_POST['action'];

	try {
		$s3 = S3Client::factory(
			array(
				'version' => 'latest',
				'region'  => 'eu-central-1'
			)
		);
	} catch (Exception $e) {
		die("Error 2: " . $e->getMessage());
	}

	switch ($action){
		case "UPLOAD":
			// Upload an object to the bucket.
			$pathInS3 = 'https://s3.us-east-2.amazonaws.com/' . $bucketName . '/' . $path  . $keyName;
			try {
				$file = $_FILES["fileToUpload"]['tmp_name'];
				$s3->putObject(
					array(
						'Bucket'=>$bucketName,
						'Key'    => $path.$keyName,
						'SourceFile' => $_FILES['uploadfile']['tmp_name'],
						'ContentType' => $_FILES['uploadfile']['type'],
						'ACL' => 'public-read',
						'StorageClass' => 'REDUCED_REDUNDANCY',
					)
				);
				$_file = "https://resource.resales-online.com/".$path  . $keyName;;
				echo "1?".$_file;
			} catch (S3Exception $e) {
				die('Error S3:' . $e->getMessage());
			} catch (Exception $e) {
				die('Error 3:' . $e->getMessage());
			}
		break;
		case "DELETE":
			// Delete an object from the bucket.
			try {
				$s3->deleteObject([
					'Bucket' => $bucketName,
					'Key'    => $keyName,
				]);
			} catch (S3Exception $e) {
				die('Error S3:' . $e->getMessage());
			} catch (Exception $e) {
				die('Error 3:' . $e->getMessage());
			}
		break;
	};

?>